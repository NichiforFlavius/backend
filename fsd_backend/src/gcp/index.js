//https://us-central1-stoked-name-334317.cloudfunctions.net/get
//https://us-central1-stoked-name-334317.cloudfunctions.net/create
const {Datastore} = require('@google-cloud/datastore');
const { stringify } = require('querystring');
const datastore = new Datastore({
    projectId: 'stoked-name-334317',
    keyFilename: 'datastore-credential.json'
});
const kindName = 'user-log';
exports.get=async(req,res)=>{
    
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "POST","GET");
    try {
        const getMethod = datastore.createQuery('user-log');
        const [datastoreContent] = await datastore.runQuery(getMethod);

        res.status(200).send(datastoreContent);
    } catch (error) {
        res.status(500).send(error);
    }
}
exports.create=async(req,res)=>{
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "POST","GET");
    try {
        const database = datastore.key('user-log');
        const entity = {
            key: database,
            data: [
                {
                    name: 'start_date',
                    value: req.query.start_date
                },
                {
                    name: 'end_date',
                    value: req.query.end_date,
                },

            ],
        };
        

        await datastore.save(entity);
        res.status(200).send("Success");
    }
    catch (error) {
        res.status(500).send(error);
    }
}
