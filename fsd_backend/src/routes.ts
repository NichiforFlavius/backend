import {UserController} from "./controller/UserController";
import { authenticate } from "./middlewares/authentification";
import { checkExistingUser, checkValidUserBody } from "./middlewares/validation";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [authenticate]
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [authenticate,checkExistingUser]
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [authenticate,checkValidUserBody]
}, {
    method: "put",
    route:"/users/update/:id",
    controller: UserController,
    action: "update",
    middlewares: [authenticate,checkExistingUser]
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [authenticate,checkExistingUser]
}, {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register",
    middlewares: [checkValidUserBody]
}, {
    method: "post",
    route: "/login",
    controller: UserController,
    action: "login"
}, 
{
    method: "post",
    route: "/evaluate",
    controller: UserController,
    action: "evaluate"
}];