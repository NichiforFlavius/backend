import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { Users } from "../entity/User";
import * as jwt from "jsonwebtoken";
import { Script } from "../tensorflow/script.js";


export class UserController {

    private userRepository = getRepository(Users);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let updatedUser = await this.userRepository.findOne(request.params.id);
        updatedUser.email = request.body.email;
        updatedUser.password = request.body.password;
        updatedUser.username = request.body.username;
        return await this.userRepository.save(updatedUser);
    }

    async register(request: Request, response: Response, next: NextFunction){
        let users = (  await this.userRepository.find({
                where: [
                    {username: request.body.username},
                    {email: request.body.email },
                ],
            })
        ).length > 0;
        if (users) {
            return "Credential Error: Username or Email already exist";
        }
        this.userRepository.save(request.body);
        return "Succesfully register!";
    }

    async login(request: Request, response: Response, next: NextFunction){

        let login = {
            email: request.body.email,
            password: request.body.password
        }


        let result =(await this.userRepository.findOne({where: login}));
        if(!result){
            response.status(401).send("Credentials mismatch!");
            return ;
        }

        var token = jwt.sign({result}, process.env.TOKEN_SECRET, { algorithm: process.env.TOKEN_ALGORITHM});
        response.status(200).send(token);

    }

    async evaluate (request: Request, response: Response, next: NextFunction){
        var buf = request.files.image.data;
        var ab = new ArrayBuffer(buf.length); 
        var view = new Uint8Array(ab); 
        for (var i = 0; i < buf.length; ++i) 
        { 
            view[i] = buf[i]; 
        } 
      
        console.log(Script.doPrediction(ab));
        //return ab;
    }
}


///Comment