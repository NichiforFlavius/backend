import {NextFunction, Request, Response} from "express";
import { getRepository } from "typeorm";
import { Users } from "../entity/User";

export async function checkExistingUser( request: Request, response: Response, next: NextFunction) {
    let userRepository = getRepository(Users);

    if (Number.isNaN(Number(request.params.id))) {
        response.status(400).send("A valid user id is needed here");
        return;
    }
    let userToRemove = await userRepository.findOne(request.params.id);
    if (!userToRemove) {
        response.status(404).send(`Cannot find user with id ${request.params.id} `);
        return;
    }
    return next();
}

export async function checkValidUserBody(request: Request, response: Response, next: NextFunction) {
    console.log(request);
    if (!request.body.username || !request.body.email || !request.body.password) {
      response.status(400).send("Not all necessary fileds were filled");
      return;
    }
    return next();
  }