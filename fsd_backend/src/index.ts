import "reflect-metadata";
import {createConnection, QueryResult} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response,NextFunction} from "express";
import {Routes} from "./routes";
import * as cors from "cors";
import * as fileUpload from "express-fileupload";

createConnection().then(async connection => {

    // create express app
    const allowedOrigins = ["http://localhost:3000"];
    const options : cors.CorsOptions = {
        origin : allowedOrigins
    }
    const app = express();
    app.use(cors(options));
    app.use(bodyParser.json());
    var router = express.Router();

    router.use("/", (request: Request, response: Response, next: NextFunction) => {
        console.log(`[${request.method}]: ${request.originalUrl}`);
        next();
    });

    app.use(fileUpload({
        useTempFiles : false,
        tempFileDir : '/tmp/'
    }));

    // register express routes from defined application routes
    Routes.forEach((route) => {
        if (route.middlewares) {
            route.middlewares.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware);
            });
        }
        (router as any)[route.method](
            route.route,
            (request: Request, response: Response, next: NextFunction) => {
                const result = new (route.controller as any)()[route.action](request, response, next);
                if (result instanceof Promise) {
                    result.then((result) => 
                        result !== null && result !== undefined
                        ? response.send(result)
                        : undefined);
                } else if (result !== null && result !== undefined) {
                    response.json(result);
                }
            }
        );
    });

    app.use("/", router);
    app.listen(3001);
    console.log("Express server has started on port 3001. Open http://localhost:3001/users to see results");
//comm
}).catch(error => console.log(error));